package com.abdelalimallam.bestmarket.dagger;




import com.abdelalimallam.bestmarket.ui.category.CategoryActivity;
import com.abdelalimallam.bestmarket.ui.category.CateogriesModule;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsActivity;
import com.abdelalimallam.bestmarket.ui.categoryApp.CateogriesAppsModule;
import com.abdelalimallam.bestmarket.ui.countries.CountriesActivity;
import com.abdelalimallam.bestmarket.ui.countries.CountriesModule;
import com.abdelalimallam.bestmarket.ui.details.DetailsActivity;
import com.abdelalimallam.bestmarket.ui.details.DetailsModule;
import com.abdelalimallam.bestmarket.ui.subcategory.SubCategoryActivity;
import com.abdelalimallam.bestmarket.ui.subcategory.SubCateogriesModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be AppComponent. The beautiful part about this setup is that you never need to tell AppComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 4 subcomponents for us.
 */
@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = CountriesModule.class)
    abstract CountriesActivity countriesActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = CateogriesModule.class)
    abstract CategoryActivity categoryActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SubCateogriesModule.class)
    abstract SubCategoryActivity subCategoryActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = CateogriesAppsModule.class)
    abstract CategoryAppsActivity categoryAppsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = DetailsModule.class)
    abstract DetailsActivity detailsActivity();



/*    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginModule.class)
    abstract LoginActivity loginActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = RegisterationModule.class)
    abstract RegisterationActivity registerationActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = ProfileModule.class)
    abstract ProfileActivity profileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = DriversModule.class)
    abstract DriversContactActivity driversContactActivity();*/


/*    @ActivityScoped
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivity();*/


//    @ActivityScoped
//    @ContributesAndroidInjector(modules = HomeModule.class)
//    abstract HomeActivity homeActivity();
//
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = SubCategoryModule.class)
//    abstract SubCategoryActivity subCategoryActivity();
//
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = ItemsListModule.class)
//    abstract ItemsListActivity itemsListActivity();


}