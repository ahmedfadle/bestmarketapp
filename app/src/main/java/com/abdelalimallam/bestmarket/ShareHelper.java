/*
package com.abdelalimallam.bestmarket;

import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;

public class ShareHelper {
    Context context;
    String subject;
    String body;
    Facebook facebook;
    public ShareHelper(Context context, String subject, String body) {
        this.context = context;
        this.subject = subject;
        this.body = body;
        facebook = null;
    }

    public Facebook share() {
        Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        List activities = context.getPackageManager().queryIntentActivities(sendIntent, 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Share with...");
        final ShareIntentListAdapter adapter = new ShareIntentListAdapter((Activity)context, R.layout.basiclistview, R.id.text1, activities.toArray());
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ResolveInfo info = (ResolveInfo) adapter.getItem(which);
                if(info.activityInfo.packageName.contains("facebook")) {
                    new PostToFacebookDialog(context, body).show();
                } else {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    intent.putExtra(Intent.EXTRA_TEXT, body);
                    ((Activity)context).startActivity(intent);
                }
            }
        });
        builder.create().show();
        return facebook;
    }
}*/
