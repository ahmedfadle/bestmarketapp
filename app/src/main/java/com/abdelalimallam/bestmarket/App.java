package com.abdelalimallam.bestmarket;


import android.content.Context;
import android.support.multidex.MultiDex;


import com.abdelalimallam.bestmarket.dagger.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;


/**
 * Created by omar on 17/08/17.
 */

public class App extends DaggerApplication   {
    public static int adCounter = 1;
    private static App instance;

    public static App getInstance() {
        return instance;
    }

/*    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        inject context
        ApplicationContextInjector.setApplicationContext(this);

    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }


}
