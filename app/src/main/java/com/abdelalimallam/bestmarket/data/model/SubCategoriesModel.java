package com.abdelalimallam.bestmarket.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoriesModel {

    @SerializedName("id")
    @Expose
    private int id;



    @SerializedName("name")
    @Expose
    private String name;






    @SerializedName("sub_sub_category_id")
    @Expose
    int subSubCategoryId;


    public int getSubSubCategoryId() {
        return subSubCategoryId;
    }

    public void setSubSubCategoryId(int subSubCategoryId) {
        this.subSubCategoryId = subSubCategoryId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
