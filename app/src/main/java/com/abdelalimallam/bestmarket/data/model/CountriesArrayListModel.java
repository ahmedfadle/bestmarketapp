package com.abdelalimallam.bestmarket.data.model;

import java.util.ArrayList;
import java.util.List;

public class CountriesArrayListModel {

    public List<CountriesModel> getCountriesistResponse() {
        return countriesistResponse;
    }

    public void setCountriesistResponse(List<CountriesModel> countriesistResponse) {
        this.countriesistResponse = countriesistResponse;
    }

    private List<CountriesModel> countriesistResponse;

}
