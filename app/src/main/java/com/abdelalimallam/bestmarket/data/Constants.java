package com.abdelalimallam.bestmarket.data;

/**
 * Created by omar on 17/08/17.
 */

public class Constants {


   // public static final String BASE_URL = "http://fitnessksa.com/";
   //public static final String BASE_URL = "http://albwabaalshamelah.com/";
 public static final String BASE_URL = "http://albwabaalshamelah.com/";
    public static final String ADD_URL = "http://albwabaalshamelah.com/public/public/web-form/";


    public static final String BASE_IMAGE_URL = "http://albwabaalshamelah.com/public/images/";

    // date format
    public static final String API_DATE_FORMATE = "yyyy-MM-dd HH:mm:ss";
    public static final String APP_DATE_FORMATE = "yyyy-MM-dd / h:mm:ss aa";


    // Fonts
    public static final String REGULAR_FONT = "font.otf";
    public static final String LIGHT_FONT = "Cairo-Light.ttf";
    public static final String BOLD_FONT = "Cairo-SemiBold.ttf";
    public static final String FrutigerLTArabic_FONT = "FrutigerLTArabic-55Roman.ttf";

    public static String SHARE_APP_LINK="https://play.google.com/store/apps/details?id=com.abdelalimallam.bestmarket";


}
