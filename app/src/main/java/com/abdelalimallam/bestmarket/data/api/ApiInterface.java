package com.abdelalimallam.bestmarket.data.api;


import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.data.model.CountriesArrayListModel;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.abdelalimallam.bestmarket.data.model.SubSubCategoriesModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("public/api/country/")
    Observable<List<CountriesModel>> getCountries();


    @GET("public/api/category/{id}/")
    Observable<List<CategoriesModel>> getCategories(@Path("id") String id);


    @GET("public/api/posts/{id}/")
    Observable<CategoryAppsModel> getPostById(@Path("id") String id);


    @GET("public/api/sub-categories/{id}/")
    Observable<List<SubCategoriesModel>> getSubCategories(@Path("id") String id);


    @GET("public/api/sub-sub-category/{id}/")
    Observable<List<SubSubCategoriesModel>> getSubSubCategories(@Path("id") String id);


    @POST("public/api/list-items")
    Observable<List<CategoryAppsModel>> getItems(@Query("subCategory") String subCategory,
                                                 @Query("subSubCategory") String subSubCategory);




}
