package com.abdelalimallam.bestmarket.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryAppsModel implements Serializable {


    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("image")
    @Expose
    String image;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("details")
    @Expose
    String details;

    @SerializedName("facebook_link")
    @Expose
    String facebook;

    @SerializedName("twitter_link")
    @Expose
    String twitter;

    @SerializedName("instagram_link")
    @Expose
    String instegram;

    @SerializedName("website_link")
    @Expose
    String website;

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    @SerializedName("youtube_link")
    @Expose
    String youtube;



    @SerializedName("android_store_link")
    @Expose
    String androidStoreLink;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstegram() {
        return instegram;
    }

    public void setInstegram(String instegram) {
        this.instegram = instegram;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAndroidStoreLink() {
        return androidStoreLink;
    }

    public void setAndroidStoreLink(String androidStoreLink) {
        this.androidStoreLink = androidStoreLink;
    }
}
