package com.abdelalimallam.bestmarket.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "best_market_db";

    // Contacts table name
    private static final String TABLE_FAV = "Favourte";

    //common colums
    private static final String KEY_ID = "id";
    private static final String KEY_PRODUCT_ID = "product_id";


    //Create Table
    private static String CREATE_TABLE_FAV = "CREATE TABLE " + TABLE_FAV + "("
            + KEY_ID + " integer primary key autoincrement,"+ KEY_PRODUCT_ID + " TEXT)";





    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_FAV);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_FAV);


        // Create tables again
        onCreate(db);

    }




    // Adding new product to basket
    public void addProductToFavouritet(String productId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID,productId); //
        Log.d("sqlId",String.valueOf(productId));

        // Inserting Row
        db.insert(TABLE_FAV, null, values);
        db.close(); // Closing database connection
    }


    // Deleting single product
    public void deleteSingleFavouriteProduct(String pos) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAV, KEY_PRODUCT_ID+ " = ?",
                new String[] { String.valueOf(pos) });
        db.close();
    }


    public int getFavCount() {

        String countQuery = "SELECT  * FROM " + TABLE_FAV;
        Cursor cursor=null;
        try{

            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(countQuery, null);

            return cursor.getCount();
            //cursor.close();

        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }
        // return count

    }


    // Getting All favourites
    public List<String> getAllFavouriteProduct() {
        List<String> list = new ArrayList<String>();
        Cursor cursor=null;
        //  List<String> contactList = new ArrayList<String>();

        try
        {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_FAV;

            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(1));
                //    Log.d("sqlIDDDDDD",String.valueOf(cursor.getInt(1)));

                } while (cursor.moveToNext());
            }

            cursor.close();

        } finally {
            // this gets called even if there is an exception somewhere above
            if(cursor != null)
                cursor.close();
        }

        // return contact list
        return list;
    }





}
