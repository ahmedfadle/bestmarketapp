package com.abdelalimallam.bestmarket.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.robertsimoes.shareable.Shareable;

import java.util.List;


public final class DialogUtil {
    public static Dialog dialog; // dialog of language selection

    private DialogUtil() {
    }

    public static ProgressDialog showProgressDialog(Context context, String message, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.show();
        return dialog;
    }

    public static ProgressDialog showProgressDialog(Context context, int message, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(message));
        dialog.setCancelable(cancelable);
        dialog.show();
        return dialog;
    }


    public static void showAlertDialog(Context context, int message,
                                       DialogInterface.OnClickListener ClickListener) {
        // create the dialog builder & set message
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(context.getString(message));

        // add negative click listener
        dialogBuilder.setPositiveButton(context.getString(R.string.text_yes), ClickListener);
        // add new click listener to dismiss the dialog
        dialogBuilder.setNegativeButton(context.getString(R.string.text_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the dialog

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
    public static String content="";

    public static void shareDialoig(final Context context, final String from, final CategoryAppsModel data)
    {

        final Activity activity= (Activity) context;

        if (from.equals("details")) {
            content=data.getTitle()+"\n"+
                    data.getDetails()+"\n"+
                    data.getFacebook()+"\n"+
                    data.getTwitter()+"\n"+
                    data.getInstegram()+"\n"+
                    data.getYoutube()+"\n"+
                    data.getWebsite()+"\n";
        }else {

            content=Constants.SHARE_APP_LINK;
        }

        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        View v=activity.getLayoutInflater().inflate(R.layout.dialog_share,null);
        ImageView ivFace= (ImageView) v.findViewById(R.id.iv_fb);
        ImageView ivInstegram= (ImageView) v.findViewById(R.id.iv_inst);
        ImageView ivTwitter= (ImageView) v.findViewById(R.id.iv_twitter);
        ImageView ivwhatsapp= (ImageView) v.findViewById(R.id.iv_whatsapp);





        ivFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {





                if (!ShareDialog.canShow(ShareLinkContent.class)) {
                    return;
                }else {
                    String url="";
                    if (data.getFacebook() == null || data.getFacebook().equals("")) {

                        url=data.getFacebook();
                    }else if (data.getTwitter() == null || data.getTwitter().equals("")) {

                        url=data.getTwitter();
                    }
                    else if (data.getInstegram() == null || data.getInstegram().equals("")) {

                        url=data.getInstegram();
                    }
                    else if (data.getWebsite() == null || data.getWebsite().equals("")) {

                        url=data.getWebsite();
                    }else if (data.getYoutube() == null || data.getYoutube().equals("")) {

                        url=data.getYoutube();
                    }
                    else if (data.getAndroidStoreLink() == null || data.getAndroidStoreLink().equals("")) {

                        url=data.getAndroidStoreLink();
                    }

                    if (from.equals("details")) {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                //.setContentTitle(data.getTitle())
                                // .setContentDescription(data.getDetails())
                                .setContentUrl(Uri.parse(url))
                                .setQuote(data.getTitle() + "\n" + data.getDetails())
                                .build();
                        ShareDialog.show(activity, content);

                    }else {
                        ShareLinkContent content = new ShareLinkContent.Builder()
                                //.setContentTitle(data.getTitle())
                                // .setContentDescription(data.getDetails())
                                .setContentUrl(Uri.parse(Constants.SHARE_APP_LINK))
                                .setQuote("البوابه الشامله")
                                .build();
                        ShareDialog.show(activity, content);
                    }
                }



                dialog.dismiss();

            }
        });

        ivInstegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (GeneralUtils.isPackageInstalled(context,"com.instagram.android"))
                {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.putExtra(Intent.EXTRA_STREAM,Uri.parse(Constants.BASE_IMAGE_URL + "/posts/" + data.getImage()));
                    shareIntent.putExtra(Intent.EXTRA_TEXT,content);
                    shareIntent.setPackage("com.instagram.android");
                    context.startActivity(shareIntent);

                }
                else {
                    Toast.makeText(context, "غير مثبت عندك", Toast.LENGTH_SHORT).show();


                }


            }
        });


        ivTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (UtilTwitter.isPackageExisted(context, "com.twitter.android")) {

                    //Uri uri =  Uri.parse("http://52.203.4.170/images/products/1524129865.jpg");
                    Shareable imageShare = new Shareable.Builder(context)

                            .message(content)
                            //   .image(uri)
                            // .url("http://" + Constants.IP_ADDRESS + "/index.php/" + UserController.getLanguageKey() + "/products/" + id)
                            .socialChannel(Shareable.Builder.TWITTER)
                            .build();
                    imageShare.share();
                }
                else {

                    Intent shareIntent;
                    String tweetUrl = "https://twitter.com/intent/tweet?text=" + content;
                    Uri uri = Uri.parse(tweetUrl);
                    shareIntent = new Intent(Intent.ACTION_VIEW, uri);

                    context.startActivity(shareIntent);
                }


                dialog.dismiss();

            }
        });


        ivwhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (GeneralUtils.isPackageInstalled(context,"com.whatsapp"))
                {

                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("text/plain");
                    whatsappIntent.setPackage("com.whatsapp");
                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, content);
                    try {
                        activity.startActivity(whatsappIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                    }
                }
                else {
                    Toast.makeText(context, "غير مثبت عندك", Toast.LENGTH_SHORT).show();
                    //shareDirect(content, context);
                }

            }
        });



        builder.setView(v);
        dialog=builder.create();
        dialog.show();

    }

    public static void shareDirect(String content,Context context)
    {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
        context.startActivity(shareIntent);
    }
}
