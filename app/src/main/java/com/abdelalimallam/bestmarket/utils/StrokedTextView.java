package com.abdelalimallam.bestmarket.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;


/**
 * Created by Adelalim on 12/13/2016.
 */

public class StrokedTextView extends TextView {

    private boolean stroke = false;
    private float strokeWidth = 0.0f;
    private int strokeColor;

    public StrokedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        initView(context, attrs);
    }

    public StrokedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initView(context, attrs);

    }

    public StrokedTextView(Context context) {
        super(context);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        stroke = a.getBoolean(R.styleable.CustomTextView_textStroke, false);
        strokeWidth = a.getFloat(R.styleable.CustomTextView_textStrokeWidth, 0.0f);
        strokeColor = a.getColor(R.styleable.CustomTextView_textStrokeColor, 0xffffffff);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/font2.otf");
        setTypeface(tf, 1);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (stroke) {
            ColorStateList states = getTextColors();
            getPaint().setStyle(Paint.Style.STROKE);
            getPaint().setStrokeWidth(strokeWidth);
            setTextColor(strokeColor);
            super.onDraw(canvas);

            getPaint().setStyle(Paint.Style.FILL);
            setTextColor(states);
        }

        super.onDraw(canvas);

    }

}