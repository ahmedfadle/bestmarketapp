package com.abdelalimallam.bestmarket.utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Calendar;


/**
 * Created by omaraboulfotoh on 9/26/17.
 */

public class GeneralUtils {

    public static boolean checkPermission(Context context, String[] permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
//        checkNotNull(fragmentManager);
//        checkNotNull(fragment);

        String TAG = fragment.getClass().getSimpleName();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment,TAG);
        transaction.commit();

       /* transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();*/
    }


    private static Gson gson;

     public static Gson getGsonParser() {
        if(null == gson) {
            GsonBuilder builder = new GsonBuilder();
            gson = builder.create();
        }
        return gson;
    }

    public static boolean isValidDateOfBirth(Calendar date) {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(1), cal.get(2), cal.get(5), 0, 0, 0);
        if (cal.compareTo(date) == 1) {
            return true;
        }
        return false;
    }



    public static String datePicker(final Context context)
    {
        final String[] dateSelectedDOB = {""};

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {







                            Calendar tmp = Calendar.getInstance();
                            tmp.set(year, monthOfYear, dayOfMonth);
                        dateSelectedDOB[0]=dayOfMonth +"-"+(monthOfYear+1) +"-"+year;

                                //dateSelectedDOB[0] =dateSelected;



                        }




                }, mYear, mMonth, mDay);
        datePickerDialog.show();


        return dateSelectedDOB[0];

    }

    public static boolean isPackageInstalled(Context context, String packageName)
    {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Tag",packageName + " not installed");
        }
        return false;
    }
}
