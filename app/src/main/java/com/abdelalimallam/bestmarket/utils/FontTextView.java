package com.abdelalimallam.bestmarket.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {

	public FontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		isInEditMode();
	}

	public FontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FontTextView(Context context) {
		super(context);
		init();
	}

	public void init() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/font2.otf");
		setTypeface(tf, 1);

	}

}
