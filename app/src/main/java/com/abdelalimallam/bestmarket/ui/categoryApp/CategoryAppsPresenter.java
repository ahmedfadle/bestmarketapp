package com.abdelalimallam.bestmarket.ui.categoryApp;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.DatabaseHandler;
import com.abdelalimallam.bestmarket.data.api.RestClient;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.abdelalimallam.bestmarket.data.model.SubSubCategoriesModel;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class CategoryAppsPresenter implements CategoryAppsContract.Presenter {

    private CategoryAppsContract.View mView;

    @Inject
    CategoryAppsPresenter() {

    }




    @Override
    public void registerView(CategoryAppsContract.View view) {
        this.mView = view;
    }

    @Override
    public void unregisterView() {

        this.mView = DUMMY_VIEW;
    }

    @Override
    public void start() {

    }

    private CategoryAppsContract.View DUMMY_VIEW = new CategoryAppsContract.View() {
        @Override
        public void loadCategoryAppsList(List<CategoryAppsModel> CategoryListResponse) {

        }

        @Override
        public void showNoDataLoaded() {

        }

        @Override
        public void navigateToDetails() {

        }

        @Override
        public void hideErrors() {

        }

        @Override
        public void setPresenter(CategoryAppsContract.Presenter presenter) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(String error) {

        }

        @Override
        public void showError() {

        }

        @Override
        public void showNoConnection() {

        }
    };





    @Override
    public void CategoryAppsList(final Context context, String SubCategorId, String SubSubCategorId) {



        mView.showProgress();

        RestClient.getClient().getItems(SubCategorId,SubSubCategorId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CategoryAppsModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<CategoryAppsModel> response) {

                        if (response.size() > 0)
                        {

                            //Toast.makeText(context,"" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                            Log.d("Apppps", String.valueOf(response.get(0).getTitle()));
                            mView.loadCategoryAppsList(response);
                            mView.hideProgress();

                        }
                        else
                        {
                            mView.hideProgress();

                            mView.showNoDataLoaded();
                            // Toast.makeText(context, "no data", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof IOException) {
                            //handle network error
                            mView.showNoConnection();
                            mView.hideProgress();
                            Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } else if (e instanceof HttpException) {
                            //handle HTTP error response code
                        } else {
                            //handle other exceptions
                        }
                    }

                    @Override
                    public void onComplete() {

                        // mView.hideProgress();
                        mView.hideProgress();
                    }
                });




    }

    List<CategoryAppsModel> CategoryListResponse=new ArrayList<>();
    @Override
    public void CategoryAppsFavList(final Context context, String id) {


        mView.showProgress();

        RestClient.getClient().getPostById(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CategoryAppsModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CategoryAppsModel response) {

                        if (response != null)
                        {

                            //Toast.makeText(context,"" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                            Log.d("AppppsResponse", String.valueOf(response));
                            CategoryListResponse.add(response);
                            mView.loadCategoryAppsList(CategoryListResponse);


                        }
                        else
                        {
                            mView.hideProgress();

                            mView.showNoDataLoaded();
                            // Toast.makeText(context, "no data", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof IOException) {
                            //handle network error
                            mView.showNoConnection();
                            mView.hideProgress();
                            Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } else if (e instanceof HttpException) {
                            //handle HTTP error response code
                        } else {
                            //handle other exceptions
                        }
                    }

                    @Override
                    public void onComplete() {

                        DatabaseHandler db=new DatabaseHandler(context);;

                        if (db.getFavCount() == CategoryListResponse.size())
                        {
                            mView.hideProgress();

                        }
                        // mView.hideProgress();
                    }
                });
    }
}
