package com.abdelalimallam.bestmarket.ui.categoryApp;


import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.ui.base.BasePresenter;
import com.abdelalimallam.bestmarket.ui.base.BaseView;

import java.util.ArrayList;
import java.util.List;


public interface CategoryAppsContract {

    interface View extends BaseView<Presenter> {
        void loadCategoryAppsList(List<CategoryAppsModel> CategoryListResponse);
        void showNoDataLoaded();
        void navigateToDetails();
        void hideErrors();
    }

    interface Presenter extends BasePresenter<View> {

        void CategoryAppsList(Context context,String SubCategorId,String SubSubCategorId);
        void CategoryAppsFavList(Context context,String id);





    }

}
