package com.abdelalimallam.bestmarket.ui.details;


import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.DetailsModel;
import com.abdelalimallam.bestmarket.ui.base.BasePresenter;
import com.abdelalimallam.bestmarket.ui.base.BaseView;

import java.util.ArrayList;


public interface DetailsContract {

    interface View extends BaseView<Presenter> {
        void loadDetails(DetailsModel data);
        void showNoDataLoaded();
        void hideErrors();
    }

    interface Presenter extends BasePresenter<View> {

        void appsDetails(Context context);



    }

}
