package com.abdelalimallam.bestmarket.ui.countries;


import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.ui.base.BasePresenter;
import com.abdelalimallam.bestmarket.ui.base.BaseView;


import java.util.ArrayList;
import java.util.List;


public interface CountriesListContract {

    interface View extends BaseView<Presenter> {
        void loadCountriesList(List<CountriesModel> CountriesListResponse);
        void showNoDataLoaded();
        void navigateToCategory();
        void hideErrors();
    }

    interface Presenter extends BasePresenter<View> {

        void countriesLis(Context context);



    }

}
