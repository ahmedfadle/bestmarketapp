package com.abdelalimallam.bestmarket.ui.subcategory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.abdelalimallam.bestmarket.ui.addItem.WebActivity;
import com.abdelalimallam.bestmarket.ui.base.BaseFragment;
import com.abdelalimallam.bestmarket.ui.category.CategoiesAdapter;
import com.abdelalimallam.bestmarket.ui.category.CategoryActivity;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsActivity;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListContract;
import com.abdelalimallam.bestmarket.utils.DialogUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SubCategoryFragment extends BaseFragment implements SubCategoryContract.View {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Unbinder unbinder;

    @BindView(R.id.ll_internet)
    LinearLayout ll_internet;
    @BindView(R.id.textView_no_data)
    TextView tvNoData;



    @BindView(R.id.ImageView_img_back)
    ImageView ivBack;

    @BindView(R.id.ImageView_img_fav)
    ImageView ivFav;
    @BindView(R.id.TextView_cat_title)
    TextView tvTitle;


    @BindView(R.id.RecyclerView_list)
    RecyclerView mRecyclerView;

    GridLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;


    @OnClick({R.id.ImageView_img_fav,R.id.ImageView_img_back,R.id.TextView_cat_sub_title,R.id.btn_retry,R.id.ImageView_img_share})
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case R.id.ImageView_img_share:

                DialogUtil.shareDialoig(getActivity(),"other",new CategoryAppsModel());
                break;


            case R.id.ImageView_img_back:

                getActivity().finish();

                break;

            case R.id.ImageView_img_fav:

                Intent i = new Intent(getActivity(), CategoryAppsActivity.class);
                i.putExtra("from", "fav");
                startActivity(i);


                break;

            case R.id.TextView_cat_sub_title:

                Intent web = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(Constants.ADD_URL));
                startActivity(web);
                //startActivity(new Intent(getActivity(),WebActivity.class));
                break;

            case R.id.btn_retry:

                ll_internet.setVisibility(View.GONE);
           /*     Intent intent=getActivity().getIntent();
                int id=intent.getIntExtra("id",0);
                mPresenter.subCategoryList(getActivity(),String.valueOf(id));*/


                Intent intent=getActivity().getIntent();

                int id=intent.getIntExtra("id",0);
                String name=intent.getStringExtra("title");
                String image= intent.getStringExtra("img");
                from=intent.getStringExtra("from");

                if (from!= null) {
                    if (from.equals("SubSub")) {
                        //  Toast.makeText(getActivity(), "sub sub", Toast.LENGTH_SHORT).show();
                        mPresenter.subSubCategoryList(getActivity(), String.valueOf(id));


                    }
                    else {
                        mPresenter.subCategoryList(getActivity(),String.valueOf(id));

                    }
                }
                else {
                    mPresenter.subCategoryList(getActivity(),String.valueOf(id));

                }


                break;
        }
    }

    @Inject
    SubCategoryContract.Presenter mPresenter;

    @Inject
    public SubCategoryFragment() {
        // Required empty public constructor
    }


    public static SubCategoryFragment newInstance(String param1, String param2) {
        SubCategoryFragment fragment = new SubCategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_sub_category, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.registerView(this);
        mPresenter.start();
    }

    String from;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Intent intent=getActivity().getIntent();

        int id=intent.getIntExtra("id",0);
        String name=intent.getStringExtra("title");
        String image= intent.getStringExtra("img");
        from=intent.getStringExtra("from");

        if (from!= null) {
            if (from.equals("SubSub")) {
              //  Toast.makeText(getActivity(), "sub sub", Toast.LENGTH_SHORT).show();
                mPresenter.subSubCategoryList(getActivity(), String.valueOf(id));


            }
            else {
                mPresenter.subCategoryList(getActivity(),String.valueOf(id));

            }
        }
        else {
            mPresenter.subCategoryList(getActivity(),String.valueOf(id));

        }

      //  Toast.makeText(getActivity(), ""+name, Toast.LENGTH_SHORT).show();

        tvTitle.setText(name);







        mRecyclerView.addOnItemTouchListener(new RecyclerSubCategoryTouch(getActivity(),
                mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {


                SubCategoiesAdapter.MyHolder holder =
                        (SubCategoiesAdapter.MyHolder) mRecyclerView.findViewHolderForAdapterPosition(position);

                holder.card_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //    showFragment(new HomeFragment());

                        final SubCategoriesModel dataEntity = subCategoryList.get(position);

                        if (dataEntity.getSubSubCategoryId() == 0)
                        {

                         //   startActivity(new Intent(getActivity(),CategoryActivity.class));

                            Intent i = new Intent(getActivity(), CategoryAppsActivity.class);
                            i.putExtra("id", dataEntity.getId());
                            i.putExtra("title", dataEntity.getName());
                            if (from != null) {
                                i.putExtra("from", from);
                            }else {
                                //Toast.makeText(getActivity(), "sub", Toast.LENGTH_SHORT).show();
                                i.putExtra("from", "Sub");

                            }

                            startActivity(i);
                        }
                        else {

                            Intent i = new Intent(getActivity(), SubCategoryActivity.class);
                            i.putExtra("id", dataEntity.getId());
                            i.putExtra("from", "SubSub");
                            i.putExtra("title", dataEntity.getName());

                            startActivity(i);


                        }


                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    List<SubCategoriesModel> subCategoryList=new ArrayList<>();
    @Override
    public void loadSubCategoryList(List<SubCategoriesModel> subCategoryListResponse) {

        subCategoryList=subCategoryListResponse;
        mAdapter = new SubCategoiesAdapter(getActivity(),subCategoryListResponse,"");
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void showNoDataLoaded() {

        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void navigateToApps() {

    }

    @Override
    public void hideErrors() {

    }

    @Override
    public void setPresenter(SubCategoryContract.Presenter presenter) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            mPresenter.unregisterView();

        }

    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}



class RecyclerSubCategoryTouch implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;
    private SubCategoryFragment.ClickListener clickListener;

    public RecyclerSubCategoryTouch(Context context, final RecyclerView recyclerView,
                                            final SubCategoryFragment.ClickListener clickListener) {
        this.clickListener = clickListener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null) {
                    clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
            clickListener.onClick(child, rv.getChildPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}

