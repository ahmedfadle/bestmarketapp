package com.abdelalimallam.bestmarket.ui.details;

import android.os.Bundle;
import android.view.View;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.ui.base.BaseActivity;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsFragment;
import com.abdelalimallam.bestmarket.utils.GeneralUtils;

import javax.inject.Inject;

public class DetailsActivity extends BaseActivity {

    @Inject
    DetailsFragment categoryFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment);

        GeneralUtils.addFragmentToActivity(getSupportFragmentManager(), categoryFragment, R.id.fragment_container);

    }

    @Override
    protected View getView() {
        return null;
    }
}
