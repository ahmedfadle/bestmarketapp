package com.abdelalimallam.bestmarket.ui.categoryApp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.DatabaseHandler;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.ui.base.BaseFragment;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;
import com.abdelalimallam.bestmarket.ui.subcategory.SubCategoiesAdapter;
import com.abdelalimallam.bestmarket.utils.DialogUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CategoryAppsFragment extends BaseFragment implements CategoryAppsContract.View {

    DatabaseHandler db;



    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Unbinder unbinder;

    @Inject
    CategoryAppsContract.Presenter mPresenter;


    @BindView(R.id.iv_img_apps_back)
    ImageView ivAppsBack;





    @BindView(R.id.tv_cat_title)
    TextView tvCatTitle;







    @BindView(R.id.ll_internet)
    LinearLayout ll_internet;
    @BindView(R.id.textView_no_data)
    TextView tvNoData;



    @BindView(R.id.ImageView_img_back)
    ImageView ivBack;

    @BindView(R.id.ImageView_img_fav)
    ImageView ivFav;
    @BindView(R.id.TextView_cat_title)
    TextView tvTitle;



    @BindView(R.id.iv_img_apps_share)
    ImageView ivShare;


    @BindView(R.id.RecyclerView_list)
    RecyclerView mRecyclerView;

    GridLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;


    @OnClick({R.id.ImageView_img_fav,R.id.ImageView_img_back,R.id.TextView_cat_sub_title,R.id.btn_retry,R.id.iv_img_apps_back,R.id.ImageView_img_share})
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case R.id.ImageView_img_share:

                DialogUtil.shareDialoig(getActivity(),"other",new CategoryAppsModel());
                break;
            case R.id.iv_img_apps_back:

                getActivity().finish();

                break;


            case R.id.ImageView_img_back:

                getActivity().finish();

                break;

            case R.id.ImageView_img_fav:

                Intent i = new Intent(getActivity(), CategoryAppsActivity.class);
                i.putExtra("from", "fav");
                startActivity(i);


                break;

            case R.id.TextView_cat_sub_title:


                break;

            case R.id.btn_retry:

                ll_internet.setVisibility(View.GONE);
                Intent intent=getActivity().getIntent();

                int id=intent.getIntExtra("id",0);
                String name=intent.getStringExtra("title");
                String from=intent.getStringExtra("from");

                tvTitle.setText(name);
               // Toast.makeText(getActivity(), ""+from, Toast.LENGTH_SHORT).show();

                if (from != null)
                {
                    if (from.equals("SubSub"))
                    {
                        mPresenter.CategoryAppsList(getActivity(),"0",String.valueOf(id));

                    }
                    else if (from.equals("Sub"))
                    {
                        //  Toast.makeText(getActivity(), ""+String.valueOf(id), Toast.LENGTH_SHORT).show();
                        mPresenter.CategoryAppsList(getActivity(),String.valueOf(id),"0");

                    }

                    else if (from.equals("fav"))
                    {
                        tvCatTitle.setText("المفضله");
                        loadFav();



                    }
                }

                break;
        }
    }


    @Inject
    public CategoryAppsFragment() {
        // Required empty public constructor
    }

    public static CategoryAppsFragment newInstance(String param1, String param2) {
        CategoryAppsFragment fragment = new CategoryAppsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_category_apps, container, false);
        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.registerView(this);
        mPresenter.start();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        ivShare.setVisibility(View.INVISIBLE);
        ivShare.setClickable(false);

        db = new DatabaseHandler(getActivity());

        Intent intent=getActivity().getIntent();

        int id=intent.getIntExtra("id",0);
        String name=intent.getStringExtra("title");
        String from=intent.getStringExtra("from");

       // Toast.makeText(getActivity(), "apppps  "+from, Toast.LENGTH_SHORT).show();

        tvTitle.setText(name);
       tvCatTitle.setText(name);

        if (from != null)
        {
            if (from.equals("SubSub"))
            {
                mPresenter.CategoryAppsList(getActivity(),"0",String.valueOf(id));

            }
            else if (from.equals("Sub"))
            {
              //  Toast.makeText(getActivity(), ""+String.valueOf(id), Toast.LENGTH_SHORT).show();
                mPresenter.CategoryAppsList(getActivity(),String.valueOf(id),"0");

            }

            else if (from.equals("fav"))
            {
                tvCatTitle.setText("المفضله");
                loadFav();



            }
        }




    }



    @Override
    public void loadCategoryAppsList(List<CategoryAppsModel> CategoryListResponse) {

        mAdapter = new CategoyAppsAdapter(getActivity(),CategoryListResponse);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void showNoDataLoaded() {

        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void navigateToDetails() {

    }

    @Override
    public void hideErrors() {

    }

    @Override
    public void setPresenter(CategoryAppsContract.Presenter presenter) {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            mPresenter.unregisterView();
        }
    }

    List<String> favList=new ArrayList<>();

    public void loadFav()
    {
        favList= db.getAllFavouriteProduct();
        if (favList.size() > 0) {
            tvNoData.setVisibility(View.GONE);

            for (int i=0;i<favList.size() ; i++) {

                mPresenter.CategoryAppsFavList(getActivity(), favList.get(i));

            }



        }
        else {
            tvNoData.setVisibility(View.VISIBLE);
        }
    }
}
