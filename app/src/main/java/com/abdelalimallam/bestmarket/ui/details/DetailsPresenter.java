package com.abdelalimallam.bestmarket.ui.details;

import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.DetailsModel;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsContract;

import java.util.ArrayList;

import javax.inject.Inject;

public class DetailsPresenter implements DetailsContract.Presenter {

    private DetailsContract.View mView;

    @Inject
    DetailsPresenter() {

    }





    @Override
    public void registerView(DetailsContract.View view) {
        this.mView = view;
    }

    @Override
    public void unregisterView() {

        this.mView = DUMMY_VIEW;
    }

    @Override
    public void start() {

    }

    private DetailsContract.View DUMMY_VIEW = new DetailsContract.View() {
        @Override
        public void loadDetails(DetailsModel data) {

        }

        @Override
        public void showNoDataLoaded() {

        }

        @Override
        public void hideErrors() {

        }

        @Override
        public void setPresenter(DetailsContract.Presenter presenter) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(String error) {

        }

        @Override
        public void showError() {

        }

        @Override
        public void showNoConnection() {

        }
    };





    @Override
    public void appsDetails(Context context) {

    }
}
