package com.abdelalimallam.bestmarket.ui.category;


import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.ui.base.BasePresenter;
import com.abdelalimallam.bestmarket.ui.base.BaseView;

import java.util.ArrayList;
import java.util.List;


public interface CategoryContract {

    interface View extends BaseView<Presenter> {
        void loadCategoryList(List<CategoriesModel> CategoryListResponse);
        void showNoDataLoaded();
        void navigateToSubCategory();
        void hideErrors();
    }

    interface Presenter extends BasePresenter<View> {

        void CategoryList(Context context,String id);



    }

}
