package com.abdelalimallam.bestmarket.ui.categoryApp;

import com.abdelalimallam.bestmarket.dagger.ActivityScoped;
import com.abdelalimallam.bestmarket.dagger.FragmentScoped;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;
import com.abdelalimallam.bestmarket.ui.category.CategoryFragment;
import com.abdelalimallam.bestmarket.ui.category.CategoryPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class CateogriesAppsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CategoryAppsFragment categoryAppsFragment();


    @ActivityScoped
    @Binds
    abstract CategoryAppsContract.Presenter provideCategoryAppsContract(CategoryAppsPresenter presenter);

}
