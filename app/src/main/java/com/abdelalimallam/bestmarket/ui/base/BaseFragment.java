package com.abdelalimallam.bestmarket.ui.base;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.utils.DialogUtil;

import dagger.android.support.DaggerFragment;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class BaseFragment extends DaggerFragment implements ParentView {

    private ProgressDialog progressDialog;
    private int menuId;



    @Override
    public void showProgress() {
        showProgressDialog();

    }

    @Override
    public void hideProgress() {
        hideProgressDialog();
    }

    @Override
    public void showError(String error) {
        showSnackbar(getView(), error);
    }

    @Override
    public void showError() {
        showSnackbar(getView(), R.string.error_occured);
    }

    @Override
    public void showNoConnection() {
        showSnackbar(getView(), R.string.error_connection);
    }

    protected void showProgressDialog(int message) {
        progressDialog = DialogUtil.showProgressDialog(getActivity(), message, false);
    }

    protected void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = DialogUtil.showProgressDialog(getActivity(), R.string.text_loading, false);
        }
    }

    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();

        }
        progressDialog = null;

    }

    protected void setFullScreen() {
        getActivity().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void hideInputType() {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }

    }

    public void showSnackbar(View view, String text) {
        Snackbar snack = Snackbar.make(
                view,
                text,
                Snackbar.LENGTH_LONG);
        View snackView = snack.getView();
        snackView.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        snack.show();
    }

    public void showSnackbar(View view, int text) {
        Snackbar snack = Snackbar.make(
                view,
                text,
                Snackbar.LENGTH_LONG);
        View snackView = snack.getView();
        snackView.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
        snack.show();
    }


    protected void createOptionsMenu(int menuId) {
        this.menuId = menuId;
        getActivity().invalidateOptionsMenu();
    }

    /**
     * function is used to create a menu
     */
    protected void removeOptionsMenu() {
        menuId = 0;
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menuId != 0) {
            getActivity().getMenuInflater().inflate(menuId, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(progressDialog !=null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
