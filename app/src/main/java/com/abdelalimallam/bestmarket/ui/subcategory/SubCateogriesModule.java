package com.abdelalimallam.bestmarket.ui.subcategory;

import com.abdelalimallam.bestmarket.dagger.ActivityScoped;
import com.abdelalimallam.bestmarket.dagger.FragmentScoped;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;
import com.abdelalimallam.bestmarket.ui.category.CategoryFragment;
import com.abdelalimallam.bestmarket.ui.category.CategoryPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class SubCateogriesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract SubCategoryFragment subCategoryFragment();


    @ActivityScoped
    @Binds
    abstract SubCategoryContract.Presenter provideSubCategoryContract(SubCategoryPresenter presenter);

}
