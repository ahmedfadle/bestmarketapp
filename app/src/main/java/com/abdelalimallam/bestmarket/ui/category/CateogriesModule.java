package com.abdelalimallam.bestmarket.ui.category;

import com.abdelalimallam.bestmarket.dagger.ActivityScoped;
import com.abdelalimallam.bestmarket.dagger.FragmentScoped;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListContract;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListFragment;
import com.abdelalimallam.bestmarket.ui.countries.CountriesPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class CateogriesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CategoryFragment categoryFragment();


    @ActivityScoped
    @Binds
    abstract CategoryContract.Presenter provideCategoryContract(CategoryPresenter presenter);

}
