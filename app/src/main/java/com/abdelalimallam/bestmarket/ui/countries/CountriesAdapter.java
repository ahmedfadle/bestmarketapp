package com.abdelalimallam.bestmarket.ui.countries;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.ui.category.CategoryActivity;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;


public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.MyHolder> {

    private ArrayList<CountriesModel> mData;
    private Context mContext;


    // Provide a suitable constructor (depends on the kind of dataset)
    public CountriesAdapter(Context context, List<CountriesModel> data) {
        this.mData = (ArrayList) data;

        mContext = context;

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_country, parent, false);
        return new MyHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        final CountriesModel dataEntity = (CountriesModel) mData.get(position);
        holder.TextView_countryName.setText(dataEntity.getName());
        Picasso.with(mContext).load(Constants.BASE_IMAGE_URL+"countries/" + dataEntity.getImage()).placeholder(R.drawable.img_blank).into(holder.ImageView_country);

/*

        // Parse the data here


        if (dataEntity.getImage() != null && !dataEntity.getImage().isEmpty())

*/

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyHolder extends RecyclerView.ViewHolder {
        // Your View ref
        @BindView(R.id.TextView_countryName)
        TextView TextView_countryName;
        @BindView(R.id.ImageView_country)
        ImageView ImageView_country;

        public MyHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CountriesModel dataEntity = mData.get(getPosition());

                    Intent i = new Intent(mContext, CategoryActivity.class);
                    i.putExtra("id", dataEntity.getId());
                    i.putExtra("name", dataEntity.getName());
                    i.putExtra("img", dataEntity.getImage());

                    mContext.startActivity(i);

                }
            });


        }


    }

}
