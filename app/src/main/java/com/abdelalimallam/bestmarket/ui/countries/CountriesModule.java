package com.abdelalimallam.bestmarket.ui.countries;

import com.abdelalimallam.bestmarket.dagger.ActivityScoped;
import com.abdelalimallam.bestmarket.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class CountriesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CountriesListFragment countriesListFragment();


    @ActivityScoped
    @Binds
    abstract CountriesListContract.Presenter provideCountriesListContract(CountriesPresenter presenter);

}
