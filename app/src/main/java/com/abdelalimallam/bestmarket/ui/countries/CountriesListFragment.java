package com.abdelalimallam.bestmarket.ui.countries;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.ui.base.BaseFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CountriesListFragment extends BaseFragment implements CountriesListContract.View {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    @BindView(R.id.ll_internet)
    LinearLayout ll_internet;
    @BindView(R.id.textView_no_data)
    TextView tvNoData;



    @BindView(R.id.RecyclerView_list)
    RecyclerView mRecyclerView;

    GridLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;


    @OnClick(R.id.btn_retry)
    public void onClick()
    {
        ll_internet.setVisibility(View.GONE);

        mPresenter.countriesLis(getActivity());

    }



    Unbinder unbinder;

    @Inject
    CountriesListContract.Presenter mPresenter;



    @Inject
    public CountriesListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CountriesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CountriesListFragment newInstance(String param1, String param2) {
        CountriesListFragment fragment = new CountriesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_countries_list, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.registerView(this);
        mPresenter.start();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

      //  mRecyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        mPresenter.countriesLis(getActivity());
      //
        //  interstionAds();
    }

    @Override
    public void loadCountriesList(List<CountriesModel> CountriesListResponse) {
        tvNoData.setVisibility(View.GONE);

        mAdapter = new CountriesAdapter(getActivity(),CountriesListResponse);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void showNoDataLoaded() {
        tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void navigateToCategory() {

    }

    @Override
    public void hideErrors() {

    }

    @Override
    public void setPresenter(CountriesListContract.Presenter presenter) {

    }


    @Override
    public void showError(String error) {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showNoConnection() {
        ll_internet.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            mPresenter.unregisterView();

        }

    }
    private InterstitialAd interstitialAd;
    public void interstionAds()
    {

        MobileAds.initialize(getActivity(), "ca-app-pub-4277536769798113~3447707586");
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-4277536769798113~3447707586");
        AdRequest request = new AdRequest.Builder().build();
        interstitialAd.loadAd(request);
 /*       interstitialAd.setAdListener(new AdListener(){
            public void onAdLoaded(){
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
            }
        });*/

        // Begin listening to interstitial & show ads.
        interstitialAd.setAdListener(new AdListener(){
            public void onAdLoaded(){
                interstitialAd.show();
            }
        });
    }



}
