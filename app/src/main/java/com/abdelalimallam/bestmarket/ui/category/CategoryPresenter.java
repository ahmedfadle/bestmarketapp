package com.abdelalimallam.bestmarket.ui.category;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.api.RestClient;
import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListContract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class CategoryPresenter implements CategoryContract.Presenter {

    private CategoryContract.View mView;

    @Inject
    CategoryPresenter() {

    }



    @Override
    public void registerView(CategoryContract.View view) {
        this.mView = view;
    }

    @Override
    public void unregisterView() {

        this.mView = DUMMY_VIEW;
    }

    @Override
    public void start() {

    }

    private CategoryContract.View DUMMY_VIEW = new CategoryContract.View() {


        @Override
        public void loadCategoryList(List<CategoriesModel> CategoryListResponse) {


        }

        @Override
        public void showNoDataLoaded() {

        }

        @Override
        public void navigateToSubCategory() {

        }

        @Override
        public void hideErrors() {

        }

        @Override
        public void setPresenter(CategoryContract.Presenter presenter) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(String error) {

        }

        @Override
        public void showError() {

        }

        @Override
        public void showNoConnection() {

        }
    };

    @Override
    public void CategoryList(final Context context,String id) {


        mView.showProgress();
        RestClient.getClient().getCategories(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CategoriesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<CategoriesModel> response) {

                        if (response.size() > 0)
                        {
                        //Toast.makeText(context,"" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                        Log.d("Category", String.valueOf(response.get(0).getName()));
                        mView.loadCategoryList(response);
                        }
                        else
                            {
                           // Toast.makeText(context, "no data", Toast.LENGTH_SHORT).show();

                                mView.showNoDataLoaded();
                            }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof IOException) {
                            //handle network error
                            mView.showNoConnection();
                            mView.hideProgress();
                            Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } else if (e instanceof HttpException) {
                            //handle HTTP error response code
                        } else {
                            //handle other exceptions
                        }
                    }

                    @Override
                    public void onComplete() {

                        mView.hideProgress();

                    }
                });



    }
}
