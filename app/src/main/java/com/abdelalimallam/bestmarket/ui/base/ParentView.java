package com.abdelalimallam.bestmarket.ui.base;

/**
 * Created by omaraboulfotoh on 11/25/17.
 */

public interface ParentView {
    void showProgress();

    void hideProgress();

    void showError(String error);

    void showError();

    void showNoConnection();
}
