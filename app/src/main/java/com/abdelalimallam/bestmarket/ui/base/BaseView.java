package com.abdelalimallam.bestmarket.ui.base;

public interface BaseView<T extends BasePresenter> extends ParentView {
    void setPresenter(T presenter);

}