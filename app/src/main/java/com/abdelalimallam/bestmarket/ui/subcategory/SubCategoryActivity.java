package com.abdelalimallam.bestmarket.ui.subcategory;

import android.os.Bundle;
import android.view.View;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.ui.base.BaseActivity;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListFragment;
import com.abdelalimallam.bestmarket.utils.GeneralUtils;

import javax.inject.Inject;

public class SubCategoryActivity extends BaseActivity {

    @Inject
    SubCategoryFragment countriesListFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment);

        GeneralUtils.addFragmentToActivity(getSupportFragmentManager(), countriesListFragment, R.id.fragment_container);

    }

    @Override
    protected View getView() {
        return null;
    }
}
