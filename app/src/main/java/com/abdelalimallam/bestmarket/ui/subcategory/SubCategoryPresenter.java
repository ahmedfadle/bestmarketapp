package com.abdelalimallam.bestmarket.ui.subcategory;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.api.RestClient;
import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.abdelalimallam.bestmarket.data.model.SubSubCategoriesModel;
import com.abdelalimallam.bestmarket.ui.category.CategoryContract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class SubCategoryPresenter implements SubCategoryContract.Presenter {

    private SubCategoryContract.View mView;

    @Inject
    SubCategoryPresenter() {

    }





    @Override
    public void registerView(SubCategoryContract.View view) {
        this.mView = view;

    }

    @Override
    public void unregisterView() {

        this.mView = DUMMY_VIEW;
    }

    @Override
    public void start() {

    }

    private SubCategoryContract.View DUMMY_VIEW = new SubCategoryContract.View() {


        @Override
        public void loadSubCategoryList(List<SubCategoriesModel> subCategoryListResponse) {

        }

        @Override
        public void showNoDataLoaded() {

        }

        @Override
        public void navigateToApps() {

        }

        @Override
        public void hideErrors() {

        }

        @Override
        public void setPresenter(SubCategoryContract.Presenter presenter) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(String error) {

        }

        @Override
        public void showError() {

        }

        @Override
        public void showNoConnection() {

        }
    };



    @Override
    public void subCategoryList(final Context context, String id) {

        mView.showProgress();

        RestClient.getClient().getSubCategories(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SubCategoriesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<SubCategoriesModel> response) {

                        if (response.size() > 0)
                        {
                            //Toast.makeText(context,"" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                            Log.d("Category", String.valueOf(response.get(0).getName()));
                            mView.loadSubCategoryList(response);
                        }
                        else
                        {

                            mView.showNoDataLoaded();
                           // Toast.makeText(context, "no data", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof IOException) {
                            //handle network error
                            mView.showNoConnection();
                            mView.hideProgress();
                            Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } else if (e instanceof HttpException) {
                            //handle HTTP error response code
                        } else {
                            //handle other exceptions
                        }
                    }

                    @Override
                    public void onComplete() {

                        mView.hideProgress();

                    }
                });

    }

    @Override
    public void subSubCategoryList(final Context context, String id) {
      //  Toast.makeText(context, "id", Toast.LENGTH_SHORT).show();


        mView.showProgress();

        RestClient.getClient().getSubSubCategories(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SubSubCategoriesModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<SubSubCategoriesModel> response) {

                        List<SubCategoriesModel> data=new ArrayList<>();
                        if (response.size() > 0)
                        {
                            for (int i=0;i<response.size(); i++)
                            {
                                SubCategoriesModel model=new SubCategoriesModel();
                                model.setId(response.get(i).getId());
                                model.setName(response.get(i).getName());
                                data.add(model);
                            }


                            //Toast.makeText(context,"" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                            Log.d("subCategory", String.valueOf(response.get(0).getName()));
                            mView.loadSubCategoryList(data);
                            mView.hideProgress();

                        }
                        else
                        {

                            mView.showNoDataLoaded();
                            // Toast.makeText(context, "no data", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof IOException) {
                            //handle network error
                            mView.showNoConnection();
                            mView.hideProgress();
                            Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                        } else if (e instanceof HttpException) {
                            //handle HTTP error response code
                        } else {
                            //handle other exceptions
                        }
                    }

                    @Override
                    public void onComplete() {

                       // mView.hideProgress();

                        mView.hideProgress();


                    }
                });
    }
}
