package com.abdelalimallam.bestmarket.ui.categoryApp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;

import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.DatabaseHandler;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.ui.details.DetailsActivity;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoyAppsAdapter extends RecyclerView.Adapter<CategoyAppsAdapter.MyHolder> {

    DatabaseHandler db;

    private ArrayList<CategoryAppsModel> mData;
    private Context mContext;

    List<String> favList=new ArrayList<>();


    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoyAppsAdapter(Context context, List<CategoryAppsModel> data) {
        this.mData = (ArrayList) data;

        mContext = context;
        db = new DatabaseHandler(mContext);

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_app_category, parent, false);
        return new MyHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        final CategoryAppsModel dataEntity = mData.get(position);
        // Parse the data here


       favList= db.getAllFavouriteProduct();
       if (favList.size() > 0)
       {
           if (favList.contains(String.valueOf(dataEntity.getId()))) {
               holder.ivFavCatApp.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_gold_star));
           }else {
               holder.ivFavCatApp.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_gray_star));

           }


       }



        holder.title.setText(dataEntity.getTitle());
        holder.info.setText(dataEntity.getDetails());

        Picasso.with(mContext).load(Constants.BASE_IMAGE_URL +"/posts/"+dataEntity.getImage()).placeholder(R.drawable.img_blank).into(holder.icon);

     /*   if (dataEntity.getImage() != null && !dataEntity.getImage().isEmpty())
            Picasso.with(mContext).load(AppConst.IMAGE_URL + dataEntity.getImage()).placeholder(R.drawable.img_blank).into(holder.icon);

*/
       // holder.rate.setText(dataEntity.getRating());
/*



        if (UserController.getFav(dataEntity.getTitle()) != null)
        {
        }






        if (dataEntity.getImage() != null && !dataEntity.getImage().isEmpty())
            Picasso.with(mContext).load(AppConst.IMAGE_URL + dataEntity.getImage()).placeholder(R.drawable.img_blank).into(holder.icon);
*/

        holder.ivFavCatApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                 if (favList.contains(String.valueOf(dataEntity.getId())))
                {
                 //   Toast.makeText(mContext, ""+String.valueOf(dataEntity.getId()), Toast.LENGTH_SHORT).show();
                    holder.ivFavCatApp.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_gray_star));
                    db.deleteSingleFavouriteProduct(String.valueOf(dataEntity.getId()));
                    favList=new ArrayList<>();
                    favList= db.getAllFavouriteProduct();



                }
                else {
                     db.addProductToFavouritet(String.valueOf(dataEntity.getId()));
                    holder.ivFavCatApp.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_gold_star));

                     favList=new ArrayList<>();
                     favList= db.getAllFavouriteProduct();
                }
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyHolder extends RecyclerView.ViewHolder {
        // Your View ref
        @BindView(R.id.icon)
        ImageView icon;

        @BindView(R.id.iv_fav_cat_app)
        ImageView ivFavCatApp;


        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.info)
        TextView info;
        @BindView(R.id.rate)
        TextView rate;
        @BindView(R.id.ll_middle)
        LinearLayout ll_middle;


        public MyHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CategoryAppsModel dataEntity = mData.get(getPosition());

                    Intent i = new Intent(mContext, DetailsActivity.class);
                    i.putExtra("details",dataEntity);

                    mContext.startActivity(i);

                }
            });

            ll_middle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CategoryAppsModel dataEntity = mData.get(getPosition());

                    Intent i = new Intent(mContext, DetailsActivity.class);
                    i.putExtra("details",dataEntity);

                    mContext.startActivity(i);

                }
            });



        }


    }

}
