package com.abdelalimallam.bestmarket.ui.subcategory;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SubCategoiesAdapter extends RecyclerView.Adapter<SubCategoiesAdapter.MyHolder> {

    private ArrayList<SubCategoriesModel> mData;
    private Context mContext;
    String country;


    // Provide a suitable constructor (depends on the kind of dataset)
    public SubCategoiesAdapter(Context context, List<SubCategoriesModel> data, String country) {
        this.mData = (ArrayList) data;

        this.country = country;
        mContext = context;

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sub_category, parent, false);
        return new MyHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        final SubCategoriesModel dataEntity = mData.get(position);
        // Parse the data here

        holder.TextView_countryName.setText(dataEntity.getName());
     /*   if (dataEntity.getImage() != null && !dataEntity.getImage().isEmpty())
            Picasso.with(mContext).load(Constants.BASE_IMAGE_URL+"categories/" + dataEntity.getImage()).placeholder(R.drawable.img_blank).into(holder.ImageView_country);
*/

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyHolder extends RecyclerView.ViewHolder {
        // Your View ref
        @BindView(R.id.TextView_countryName)
        TextView TextView_countryName;
        @BindView(R.id.ImageView_country)
        ImageView ImageView_country;


        @BindView(R.id.fl_sub_cat)
        FrameLayout flSubRow;

        @BindView(R.id.card_view)
        CardView card_view;

        public MyHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

/*
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final SubCategoriesModel dataEntity = mData.get(getPosition());

                    if (dataEntity.getSubSubCategoryId() == 0)
                    {
                        mPresenter.subSubCategoryList(mContext,String.valueOf(dataEntity.getId()));

                    }
                    else {


                    }


  *//*                  Intent i = new Intent(mContext, CategoryAppsActivity.class);
                    i.putExtra("id", dataEntity.getId());
                    i.putExtra("country", country);
                    i.putExtra("title", dataEntity.getName());*//*

                 //   mContext.startActivity(i);

                }
            });*/


        }


    }

}




