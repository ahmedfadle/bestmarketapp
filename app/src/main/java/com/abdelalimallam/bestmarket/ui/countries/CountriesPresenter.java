package com.abdelalimallam.bestmarket.ui.countries;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.api.RestClient;
import com.abdelalimallam.bestmarket.data.model.CountriesArrayListModel;
import com.abdelalimallam.bestmarket.data.model.CountriesModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

public class CountriesPresenter implements CountriesListContract.Presenter {

    private CountriesListContract.View mView;

    @Inject
    CountriesPresenter() {

    }

    @Override
    public void countriesLis(final Context context) {

        mView.showProgress();


        RestClient.getClient().getCountries().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CountriesModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<CountriesModel> countriesArrayListModelResponse) {

                //if (countriesArrayListModelResponse.body().getCountriesistResponse().size() > 0) {
               //     Toast.makeText(context, "" +countriesArrayListModelResponse.size(), Toast.LENGTH_SHORT).show();
                   Log.d("Upload", String.valueOf(countriesArrayListModelResponse.get(0).getName()));

                   if (countriesArrayListModelResponse.size()>0) {
                       mView.hideProgress();

                       mView.loadCountriesList(countriesArrayListModelResponse);
                   }
                   else {
                       mView.showNoDataLoaded();
                   }
               // }

            }

            @Override
            public void onError(Throwable e) {
             //   Toast.makeText(context, "errror", Toast.LENGTH_SHORT).show();

                mView.hideProgress();
                if (e instanceof IOException) {
                    //handle network error
                    mView.showNoConnection();
                    mView.hideProgress();
                    Toast.makeText(context, context.getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                } else if (e instanceof HttpException) {
                    //handle HTTP error response code
                } else {
                    //handle other exceptions
                }

            }

            @Override
            public void onComplete() {

                mView.hideProgress();

            }
        });

    }

    @Override
    public void registerView(CountriesListContract.View view) {
        this.mView = view;
    }

    @Override
    public void unregisterView() {

        this.mView = DUMMY_VIEW;
    }

    @Override
    public void start() {

    }

    private CountriesListContract.View DUMMY_VIEW = new CountriesListContract.View() {
        @Override
        public void loadCountriesList(List<CountriesModel> CountriesListResponse) {

        }

        @Override
        public void showNoDataLoaded() {

        }

        @Override
        public void navigateToCategory() {

        }

        @Override
        public void hideErrors() {

        }

        @Override
        public void setPresenter(CountriesListContract.Presenter presenter) {

        }

        @Override
        public void showProgress() {

        }

        @Override
        public void hideProgress() {

        }

        @Override
        public void showError(String error) {

        }

        @Override
        public void showError() {

        }

        @Override
        public void showNoConnection() {

        }
    };

}
