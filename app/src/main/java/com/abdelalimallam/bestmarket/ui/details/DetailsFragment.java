package com.abdelalimallam.bestmarket.ui.details;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.DatabaseHandler;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.data.model.DetailsModel;
import com.abdelalimallam.bestmarket.ui.base.BaseFragment;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsContract;
import com.abdelalimallam.bestmarket.utils.DialogUtil;
import com.abdelalimallam.bestmarket.utils.GeneralUtils;
import com.abdelalimallam.bestmarket.utils.StrokedTextView;
import com.abdelalimallam.bestmarket.utils.UtilTwitter;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.robertsimoes.shareable.Shareable;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class DetailsFragment extends BaseFragment implements DetailsContract.View  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    DatabaseHandler db;
    List<String> favList=new ArrayList<>();


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Unbinder unbinder;


    @BindView(R.id.iv_youtube)
    ImageView iv_youtube;

    @BindView(R.id.iv_facebook)
    ImageView iv_facebook;

    @BindView(R.id.iv_instagram)
    ImageView iv_instagram;

    @BindView(R.id.iv_twitter)
    ImageView iv_twitter;

    @BindView(R.id.iv_playstore)
    ImageView iv_playstore;

    @BindView(R.id.iv_website)
    ImageView iv_website;

    CategoryAppsModel data =new CategoryAppsModel();

    @OnClick({R.id.iv_facebook ,R.id.iv_instagram ,R.id.iv_twitter ,R.id.iv_playstore ,R.id.iv_website,R.id.iv_fav,R.id.iv_img_apps_back,R.id.iv_youtube,R.id.iv_img_apps_share})
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_fav:


                if (favList.contains(String.valueOf(data.getId())))
                {
                    ivFav.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_rate));
                    db.deleteSingleFavouriteProduct(String.valueOf(data.getId()));

                    favList=new ArrayList<>();
                    favList= db.getAllFavouriteProduct();

                }
                else {
                    db.addProductToFavouritet(String.valueOf(data.getId()));
                    ivFav.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_gold_star));


                    favList=new ArrayList<>();
                    favList= db.getAllFavouriteProduct();
                }


                break;

            case R.id.iv_facebook:

                openLink(String.valueOf(data.getFacebook()));

                break;

            case R.id.iv_youtube:

                openLink(String.valueOf(data.getYoutube()));

                break;

            case R.id.iv_instagram  :

                openLink(String.valueOf(data.getInstegram()));

                break;


            case R.id.iv_twitter  :
                openLink(String.valueOf(data.getTwitter()));


                break;

            case R.id.iv_website  :

                openLink(String.valueOf(data.getWebsite()));

                break;

            case R.id.iv_playstore  :

                openLink(String.valueOf(data.getAndroidStoreLink()));

                break;


            case R.id.iv_img_apps_back  :
                getActivity().finish();
                break;


            case R.id.iv_img_apps_share  :


                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "البوابه الشامله");
                intent.putExtra(Intent.EXTRA_TEXT,
                        data.getTitle()+"\n"+
                        data.getDetails()+"\n"+
                        data.getFacebook()+"\n"+
                        data.getTwitter()+"\n"+
                        data.getInstegram()+"\n"+
                        data.getYoutube()+"\n"+
                        data.getWebsite()+"\n"
                );
             //   startActivity(Intent.createChooser(intent, "choose one"));

                //shareIt(this.getView());

                //ShareSub();

                String con=data.getTitle()+"\n"+
                        data.getDetails()+"\n"+
                        data.getFacebook()+"\n"+
                        data.getTwitter()+"\n"+
                        data.getInstegram()+"\n"+
                        data.getYoutube()+"\n"+
                        data.getWebsite()+"\n";
                DialogUtil.shareDialoig(getActivity(),"details",data);


       /*         Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, data.getTitle()+"\n"+
                        data.getDetails()+"\n"+
                        data.getFacebook()+"\n"+
                        data.getTwitter()+"\n"+
                        data.getInstegram()+"\n"+
                        data.getYoutube()+"\n"+
                        data.getWebsite()+"\n");
                PackageManager pm = getActivity().getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.name).contains("facebook")) {
                        final ActivityInfo activity = app.activityInfo;
                        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        shareIntent.setComponent(name);
                        startActivity(shareIntent);
                        break;
                    }
                }*/
                break;
        }
    }

    public void shareIt(View view){
        //sharing implementation
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "string of text  more text! Get the app at http://someapp.com";

        PackageManager pm = view.getContext().getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(sharingIntent, 0);
        for(final ResolveInfo app : activityList) {

            String packageName = app.activityInfo.packageName;
            Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);
            targetedShareIntent.setType("text/plain");
            targetedShareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "share");
            if(TextUtils.equals(packageName, "com.facebook.katana")){
               // Toast.makeText(getActivity(), "facebook", Toast.LENGTH_SHORT).show();
            //    targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "http://www.google.com");

                if (!ShareDialog.canShow(ShareLinkContent.class)) {
                    return;
                }else {
                    // Toast.makeText(DetailsActivity.this, ""+id, Toast.LENGTH_SHORT).show();

                    ShareLinkContent content = new ShareLinkContent.Builder()
                            // .setContentTitle("title")
                            // .setContentDescription("sss")
                            .setContentUrl(Uri.parse("http://www.google.com"))
                            .build();
                    ShareDialog.show(getActivity(), content);
                }
            } else {
                targetedShareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            }

            targetedShareIntent.setPackage(packageName);
            targetedShareIntents.add(targetedShareIntent);

        }

        Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share Idea");

        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
        startActivity(chooserIntent);

    }



    @BindView(R.id.iv_img_apps_share)
    ImageView ivShare;


    @BindView(R.id.TextView_title)
    TextView TextViewTitle;
    @BindView(R.id.ImageView_img)
    ImageView ImageViewImg;
    @BindView(R.id.iv_fav)
    ImageView ivFav;
    @BindView(R.id.appImage)
    ImageView appImage;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.info)
    TextView info;
    @BindView(R.id.rate)
    TextView rate;
    @BindView(R.id.location)
    TextView location;


    @BindView(R.id.iv_img_apps_back)
    ImageView ivAppsBack;

    @BindView(R.id.tv_cat_title)
    TextView tvCatTitle;


    @Inject
    DetailsContract.Presenter mPresenter;


    @Inject
    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(String param1, String param2) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_details, container, false);
        unbinder = ButterKnife.bind(this, v);
        FacebookSdk.sdkInitialize(this.getActivity());

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.registerView(this);
        mPresenter.start();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        db = new DatabaseHandler(getActivity());



        data = (CategoryAppsModel) getActivity().getIntent().getSerializableExtra("details");
        TextViewTitle.setText(data.getTitle());
        title.setText(data.getTitle());
        tvCatTitle.setText(data.getTitle());

        info.setText(data.getDetails());

        Log.d("image",Constants.BASE_IMAGE_URL + "posts/" + data.getImage());

        if (data.getImage() == null || data.getImage().equals(""))
        {
            Picasso.with(getActivity()).load(R.drawable.img_blank).into(appImage);

        }
        else {
            Picasso.with(getActivity()).load(Constants.BASE_IMAGE_URL + "/posts/" + data.getImage()).into(appImage);
        }

        favList= db.getAllFavouriteProduct();
        if (favList.size() > 0)
        {
            if (favList.contains(String.valueOf(data.getId()))) {
                ivFav.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_gold_star));
            }else {
                ivFav.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_rate));

            }


        }



        if (data.getFacebook() == null || data.getFacebook().equals(""))
        {
            iv_facebook.setVisibility(View.GONE);
        }

        if (data.getInstegram() == null || data.getInstegram().equals(""))
        {
            iv_instagram.setVisibility(View.GONE);
        }

        if (data.getTwitter() == null || data.getTwitter().equals(""))
        {
            iv_twitter.setVisibility(View.GONE);
        }

        if (data.getAndroidStoreLink() == null || data.getAndroidStoreLink().equals(""))
        {
            iv_playstore.setVisibility(View.GONE);
        }

        if (data.getWebsite() == null || data.getWebsite().equals(""))
        {
            iv_website.setVisibility(View.GONE);
        }

        if (data.getYoutube() == null || data.getYoutube().equals(""))
        {
            iv_youtube.setVisibility(View.GONE);
        }

        // Toast.makeText(getActivity(), ""+data.getDetails(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loadDetails(DetailsModel data) {

    }

    @Override
    public void showNoDataLoaded() {

    }


    @Override
    public void hideErrors() {

    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            mPresenter.unregisterView();
        }
    }


    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {

    }


/*    public void openLink(String url)
    {
        if (Patterns.WEB_URL.matcher(url).matches()) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.google.com"));
            startActivity(browserIntent);
        }
        else {
            Toast.makeText(getActivity(), "اللينك المتاح غير صحيح حاولا لاحقا", Toast.LENGTH_SHORT).show();
        }
    }*/


    public void openLink(String url) {

        Uri webpage = Uri.parse(url);

        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            webpage = Uri.parse("http://" + url);
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void ShareSub() {
        final Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,
                data.getTitle()+"\n"+
                data.getDetails()+"\n"+
                data.getFacebook()+"\n"+
                data.getTwitter()+"\n"+
                data.getInstegram()+"\n"+
                data.getYoutube()+"\n"+
                data.getWebsite()+"\n");

        final List<ResolveInfo> activities = getActivity().getPackageManager().queryIntentActivities (i, 0);

        List<String> appNames = new ArrayList<String>();
        for (ResolveInfo info : activities) {
            appNames.add(info.loadLabel(getActivity().getPackageManager()).toString());
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("شارك مع");
        builder.setItems(appNames.toArray(new CharSequence[appNames.size()]), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ResolveInfo info = activities.get(item);
                if (info.activityInfo.packageName.equals("com.facebook.katana")) {
                    // Facebook was chosen


                    if (!ShareDialog.canShow(ShareLinkContent.class)) {
                        return;
                    }else {
                        // Toast.makeText(DetailsActivity.this, ""+id, Toast.LENGTH_SHORT).show();

                        String url="";
                        if (data.getFacebook() == null || data.getFacebook().equals("")) {

                            url=data.getFacebook();
                        }else if (data.getTwitter() == null || data.getTwitter().equals("")) {

                            url=data.getTwitter();
                        }
                        else if (data.getInstegram() == null || data.getInstegram().equals("")) {

                            url=data.getInstegram();
                        }
                        else if (data.getWebsite() == null || data.getWebsite().equals("")) {

                            url=data.getWebsite();
                        }else if (data.getYoutube() == null || data.getYoutube().equals("")) {

                            url=data.getYoutube();
                        }
                        else if (data.getAndroidStoreLink() == null || data.getAndroidStoreLink().equals("")) {

                            url=data.getAndroidStoreLink();
                        }

                        ShareLinkContent content = new ShareLinkContent.Builder()
                                 .setContentTitle(data.getTitle())
                                 .setContentDescription(data.getDetails())
                                .setContentUrl(Uri.parse(url))
                                .build();
                        ShareDialog.show(getActivity(), content);
                    }


                } else {
                    // Twitter was chosen

                    i.setPackage(info.activityInfo.packageName);
                    startActivity(i);


                }

                // start the selected activity
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }



}
