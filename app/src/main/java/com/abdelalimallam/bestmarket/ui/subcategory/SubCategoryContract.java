package com.abdelalimallam.bestmarket.ui.subcategory;


import android.content.Context;

import com.abdelalimallam.bestmarket.data.model.SubCategoriesModel;
import com.abdelalimallam.bestmarket.ui.base.BasePresenter;
import com.abdelalimallam.bestmarket.ui.base.BaseView;

import java.util.ArrayList;
import java.util.List;


public interface SubCategoryContract {

    interface View extends BaseView<Presenter> {
        void loadSubCategoryList(List<SubCategoriesModel> subCategoryListResponse);
        void showNoDataLoaded();
        void navigateToApps();
        void hideErrors();
    }

    interface Presenter extends BasePresenter<View> {

        void subCategoryList(Context context,String id);
        void subSubCategoryList(Context context,String id);



    }

}
