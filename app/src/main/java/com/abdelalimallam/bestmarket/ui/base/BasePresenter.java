package com.abdelalimallam.bestmarket.ui.base;

public interface BasePresenter<T extends BaseView> {

    void registerView(T view);

    void unregisterView();

    void start();

}