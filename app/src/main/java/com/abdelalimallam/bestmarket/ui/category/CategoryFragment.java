package com.abdelalimallam.bestmarket.ui.category;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.data.Constants;
import com.abdelalimallam.bestmarket.data.model.CategoriesModel;
import com.abdelalimallam.bestmarket.data.model.CategoryAppsModel;
import com.abdelalimallam.bestmarket.ui.base.BaseFragment;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsActivity;
import com.abdelalimallam.bestmarket.ui.countries.CountriesAdapter;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListContract;
import com.abdelalimallam.bestmarket.utils.DialogUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CategoryFragment extends BaseFragment implements CategoryContract.View {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Unbinder unbinder;


    @BindView(R.id.ll_internet)
    LinearLayout ll_internet;
    @BindView(R.id.textView_no_data)
    TextView tvNoData;


    @BindView(R.id.ImageView_img_back)
    ImageView ivBack;
    @BindView(R.id.ImageView_img)
    ImageView ivCountry;
    @BindView(R.id.ImageView_img_fav)
    ImageView ivFav;
    @BindView(R.id.TextView_title)
    TextView tvTitle;


    @BindView(R.id.RecyclerView_list)
    RecyclerView mRecyclerView;

    GridLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;




    @OnClick({R.id.ImageView_img_fav,R.id.ImageView_img_back,R.id.btn_retry,R.id.ImageView_img_share})
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ImageView_img_back:

                getActivity().finish();

                break;

            case R.id.btn_retry:
                ll_internet.setVisibility(View.GONE);

                Intent intent=getActivity().getIntent();
                int id=intent.getIntExtra("id",0);

                mPresenter.CategoryList(getActivity(),String.valueOf(id));

                break;

            case R.id.ImageView_img_fav:

                Intent i = new Intent(getActivity(), CategoryAppsActivity.class);
                i.putExtra("from", "fav");
                startActivity(i);


                break;

            case R.id.ImageView_img_share:

                DialogUtil.shareDialoig(getActivity(),"other",new CategoryAppsModel());
                break;
        }
    }

    @Inject
    CategoryContract.Presenter mPresenter;


    @Inject
    public CategoryFragment() {
        // Required empty public constructor
    }


    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.registerView(this);
        mPresenter.start();


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Intent intent=getActivity().getIntent();
        int id=intent.getIntExtra("id",0);
        String name=intent.getStringExtra("name");
        String image= intent.getStringExtra("img");


        tvTitle.setText(name);
        Picasso.with(getActivity()).load(Constants.BASE_IMAGE_URL+"countries/" + image).placeholder(R.drawable.img_blank).into(ivCountry);






        mPresenter.CategoryList(getActivity(),String.valueOf(id));
    }

    @Override
    public void loadCategoryList(List<CategoriesModel> CategoryListResponse) {

        mAdapter = new CategoiesAdapter(getActivity(),CategoryListResponse);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void showNoDataLoaded() {

        tvNoData.setVisibility(View.VISIBLE);

    }

    @Override
    public void navigateToSubCategory() {

    }

    @Override
    public void hideErrors() {

    }

    @Override
    public void setPresenter(CategoryContract.Presenter presenter) {

    }


    @Override
    public void showError(String error) {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showNoConnection() {
        ll_internet.setVisibility(View.VISIBLE);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
            mPresenter.unregisterView();

        }

    }

}
