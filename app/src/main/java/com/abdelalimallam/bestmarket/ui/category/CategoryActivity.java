package com.abdelalimallam.bestmarket.ui.category;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abdelalimallam.bestmarket.R;
import com.abdelalimallam.bestmarket.ui.base.BaseActivity;
import com.abdelalimallam.bestmarket.ui.countries.CountriesListFragment;
import com.abdelalimallam.bestmarket.utils.GeneralUtils;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class CategoryActivity extends BaseActivity {

    @Inject
    CategoryFragment categoryFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment);

        GeneralUtils.addFragmentToActivity(getSupportFragmentManager(), categoryFragment, R.id.fragment_container);





    }

    @Override
    protected View getView() {
        return null;
    }
}
