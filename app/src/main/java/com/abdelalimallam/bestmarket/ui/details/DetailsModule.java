package com.abdelalimallam.bestmarket.ui.details;

import com.abdelalimallam.bestmarket.dagger.ActivityScoped;
import com.abdelalimallam.bestmarket.dagger.FragmentScoped;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsContract;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsFragment;
import com.abdelalimallam.bestmarket.ui.categoryApp.CategoryAppsPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class DetailsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract DetailsFragment detailsFragment();


    @ActivityScoped
    @Binds
    abstract DetailsContract.Presenter provideDetailsContract(DetailsPresenter presenter);

}
